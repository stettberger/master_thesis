#!/usr/bin/env python

import os,sys
import logging
import imp
import math
import collections
import numpy

tmp_path = "%s/git/versuchung/src"% os.environ["HOME"]
if os.path.exists(tmp_path):
    sys.path.append(tmp_path)


from versuchung.experiment import Experiment
from versuchung.types import String, Bool,List
from versuchung.database import Database
from versuchung.files import File, Directory, Executable
from versuchung.archives import GitArchive
from versuchung.execute import shell
from versuchung.tex import DatarefDict
from threading import Thread

class FailTrace(Experiment):
    inputs = {
        "coredos": GitArchive("/proj/i4gerrit/review_site/git/coredos.git"),
        "mpu": Bool(False),
    }
    outputs = {
        "traces": Directory("traces"),
    }

    def configure_and_build(self, config, directory):
        extra = ""
        name = "base"
        if self.mpu.value:
            extra += " --mpu=yes"
            name  += "+mpu"
        else:
            extra += " --mpu=no"

        if config["encode_system"]:
            extra += " --encoded=yes"
            name += "+enc"
        else:
            extra += " --encoded=no"

        if config["specialize_system"]:
            extra += " --specialize=yes"
            name  += "+opt"
        else:
            extra += " --specialize=no"


        logging.info("Configuring...")
        shell("cd %s; ../new_build_env.py -a i386 -c "+ extra, directory)

        SGFLAGS = ["-fsse", "-fConstructGlobalCFG"]
        if config["insert_state_asserts"]:
            SGFLAGS.append("-fgen-asserts")
            name += "+ass"
        if config["insert_cfg_regions"]:
            SGFLAGS.append("-fcfg-regions")
            name += "+flow"

        config["name"] = name

        logging.info("Building..." + str(config))
        logging.info(" ".join(SGFLAGS))
        shell("export SGFLAGS=%s; cd %s; make -j $(nproc) fail-trace-bench-coptermock",
              " ".join(SGFLAGS), directory)

    def run(self):
        logging.info("Cloning coredos...")

        with self.coredos as coredos_path:
            flagss = [(x & 1 > 0, x & 2 > 0, x & 4 > 0, x & 8 >0)  for x in range(0,16)]
            variants = []
            for flags in flagss:
                variants += [{"encode_system": flags[0],
                              "specialize_system": flags[1],
                              "insert_state_asserts": flags[2],
                              "insert_cfg_regions": flags[3]}]


            def VAR(variant):
                directory = "build" + str(id(variant))
                os.mkdir(directory)

                self.configure_and_build(variant, directory)
                shell("mv %s/fail-bench-coptermock %s/%s", 
                      directory,
                      self.traces.path,
                      variant["name"])

            threads = []
            for variant in variants:
                t = Thread(target = VAR, args=(variant,))
                t.start()
                t.join()

class FailImport(Experiment):
    inputs = {
        "trace": FailTrace("FailTrace"),
        "fail-tool-dir": Directory("/proj/i4danceos/tools/fail"),
        "fail-tracer": Executable("/proj/i4danceos/tools/fail/cored-tracing"),
    }

    def run(self):
        for trace in self.trace.traces:
            variant = "dosek/" + trace.basename
            print variant

            if variant in ("dosek/base+mpu",
                               "dosek/base+mpu+ass",
                               "dosek/base+mpu+opt+ass+flow",
                               ):
                continue


            for (label, importer, importer_args) in [\
                                ("mem",    "MemoryImporter", []),
                                ("regs",   "RegisterImporter", []),
                                ("ip",     "RegisterImporter", ["--no-gp", "--ip"]),
                                ("flags",  "RegisterImporter", ["--no-gp", "--flags"]),
                                            ]:
                benchmark = label
                logging.info("Importing coredos/%s", )
                cmdline = "%(path)s/import-trace -v %(variant)s -b %(benchmark)s -i %(importer)s "\
                          + "-t %(trace)s -e %(elf)s %(args)s"
                shell(cmdline %\
                      {"path": self.fail_tool_dir.path,
                       "variant": variant,
                       "benchmark": benchmark,
                       "importer": importer,
                       "trace":  os.path.join(trace.path, "trace.pb"),
                       "elf":  os.path.join(trace.path, "fail-bench-coptermock"),
                       "args": " ".join(importer_args)})
            shell("%s/prune-trace -v %s -b %% -p basic",
                  self.fail_tool_dir.path, variant)


if __name__ == "__main__":
    import shutil, sys
    if len(sys.argv) > 1:
        action = sys.argv[1]; del sys.argv[1]
    actions = {"trace": FailTrace,
               "import": FailImport}

    if action in actions:
        experiment = actions[action]()
        dirname = experiment(sys.argv)
    else:
        print "No action to be taken, use: %s" % ", ".join(actions.keys())

