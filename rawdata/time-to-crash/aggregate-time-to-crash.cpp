#include <fstream>
#include <map>
#include <vector>
#include <string>
#include <boost/algorithm/string.hpp>

using namespace std;

typedef struct {
    int Class;
    int Multiplicator;
    int SizeOfClass;
    int Rest;
} PP;


PP intlog(unsigned int i) {
    PP ret;
    ret.Class = 0;
    ret.Multiplicator = i / 1000;
    ret.SizeOfClass = 1000;
    ret.Rest = i % 1000;

    return ret;
}

int addIntervals(unsigned  * x, int Class, int Multiplicator, int value) {
    x[Multiplicator] += value;

    //cout <<  Multiplicator * 1000 << " = " << x[Multiplicator] << " (∂ " <<  value<< ")" << endl;
    
    assert(Class == 0);
    assert(Multiplicator < 100000);
    assert(value <= 1000);
    
    return value;
}

int main(int argc, char *argv[]) {

   std::vector<std::string> strs;

   // Drop first line
   std::string _dummy_;
   getline(cin, _dummy_);

   std::map<std::string, unsigned  *> Xclasses;



   for (std::string line; getline(cin, line); ) {
       boost::split(strs, line, boost::is_any_of("\t"));

       unsigned* classes = Xclasses[strs[0]];
       if (!classes) {
           classes = new unsigned [100000];
           memset(classes, 0, sizeof(classes));
           Xclasses[strs[0]] = classes;
       }

       unsigned long long t1 = std::stoi(strs[3]);
       unsigned long long t2 = std::stoi(strs[4]);
       unsigned long long tc = std::stoi(strs[5]);

       int intervals = t2 - t1 + 1;
       unsigned int smallest_interval = tc - t2 + 1;
       unsigned int largest_interval = tc - t1 + 1;

       PP small = intlog(smallest_interval);

       //cout << smallest_interval << " " << largest_interval << " ++ " << intervals<< endl;
       //cout << intervals << endl;
       PP it = small;
       int offset_in_interval = small.Rest;
       // cout << small.Rest << "----" << endl;;
       while (intervals > 0) {
           int into_current_slot = 1000;
           if (intervals < 1000) 
               into_current_slot = intervals;

           if ((into_current_slot + offset_in_interval) >= it.SizeOfClass) {
               into_current_slot = it.SizeOfClass - offset_in_interval;
           }

           offset_in_interval = 0;

           intervals -= addIntervals(classes, it.Class, it.Multiplicator, into_current_slot);

           it.Multiplicator ++;
           // if (it.Multiplicator >= 10) {
           //     it.Multiplicator = 1;
           //     it.Class ++;
           //     it.SizeOfClass *= 10;
           //}
       }

       // classes[small.Class * 10 + small.Multiplicator] += 1;
       // intervals -= 1;

       // cout << "-------------" << endl;


       // cout << ">> " << intervals << endl;

       assert(intervals == 0);
   }
   unsigned long long offset;
   for (std::map<string, unsigned  *>::iterator it = Xclasses.begin();
        it != Xclasses.end(); it++) {
       offset = 1000;
       unsigned *classes = it->second;
       for (int R = 0; R < 10000; R++) {
           std::cout << it->first << " " << (R * offset) << " " << classes[R] << endl;
       }
   }
}
