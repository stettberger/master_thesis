#!/usr/bin/env python

import os,sys
import logging
import imp
import math
import collections
import numpy

tmp_path = "%s/git/versuchung/src"% os.environ["HOME"]
if os.path.exists(tmp_path):
    sys.path.append(tmp_path)


from versuchung.experiment import Experiment
from versuchung.types import String, Bool,List
from versuchung.files import File, Directory
from versuchung.archives import GitArchive
from versuchung.execute import shell
from versuchung.tex import DatarefDict

class GatherStatsDict(Experiment):
    inputs = {
        "coredos": GitArchive("/proj/i4gerrit/review_site/git/coredos.git"),
        "fail-trace-all": Bool(False),
        "encode-system": Bool(False),
        "specialize-system": Bool(False),
        "insert-state-asserts": Bool(False),
        "insert-cfg-regions": Bool(False),
	"mpu": Bool(False),
    }
    outputs = {"dicts": Directory("dicts"),
               "stats": File("statistics.py")}

    def run(self):
        logging.info("Cloning coredos...")
        with self.coredos as coredos_path:
            self.stats.copy_contents("generator/statistics.py")

            os.mkdir("build");
            logging.info("Configuring...")
            if not self.fail_trace_all.value:
                # Activate coptermock-no-annotation
                shell("echo 'COREDOS_BINARY(NAME bench-coptermock-no-annotation SYSTEM_XML system-no-annotation.xml coredos.cc)' >> app/benchmark/coptermock/CMakeLists.txt")

                shell("cd build; ../new_build_env.py -a i386 --encoded=yes --mpu=no --specialize=yes -c")
                logging.info("Building...")
                shell("cd build; make -j $(nproc) build_and_test")
            else: # Trace all
                extra = ""
                if self.encode_system.value:
                    extra += " --encoded=yes"
                else:
                    extra += " --encoded=no"

                if self.specialize_system.value:
                    extra += " --specialize=yes"
                else:
                    extra += " --specialize=no"

                if self.mpu.value:
                    extra += " --mpu=yes"
                else:
                    extra += " --mpu=no"

                shell("cd build; ../new_build_env.py -a i386 --fail-trace-all=yes "+ extra)

                SGFLAGS = ["-fsse", "-fConstructGlobalCFG"]
                if self.insert_state_asserts.value:
                    SGFLAGS.append("-fgen-asserts")

                if self.insert_cfg_regions.value:
                    SGFLAGS.append("-fcfg-regions")

                logging.info("Building...")
                logging.info(" ".join(SGFLAGS))
                shell("export SGFLAGS=%s; cd build; make -j $(nproc) fail-trace-all",
                      " ".join(SGFLAGS))

            (stats_files, _) = shell("find build -name stats.dict.py")

            for filename in stats_files:
                name = filename.split("/")[-2]
                logging.info("Gathering for %s", name)
                f = self.dicts.new_file(name)
                f.copy_contents(filename)




class CombineStatsDict(Experiment):
    inputs = {"gather": GatherStatsDict("GatherStatsDict") }
    outputs = {"dref" : DatarefDict() }

    def fmtn(self,name):
        return name.replace("bcc1_", "").replace("_", "-")
    def key(self, keys, value):
        self.dref["/".join(["stats.dict"] + keys)] = value

    def work_on_benchmark(self, name, stats):
        def S(keys, value):
            self.key([self.fmtn(name)] + keys, value)
        sse = stats.find_one("SymbolicSystemExecution")
        ssf = stats.find_one("SystemStateFlow")
        gcfg = stats.find_one("ConstructGlobalCFG")
        gcfg["name"] = name
        # The overall precision is the mean of the SSE state
        # precisions, since it is the more precise analysis.
        gcfg["precision"] = numpy.mean(sse["precision"])
        # The complexity of a gcfg is the number of actual needed GCFG
        # edges and number of all possible edges
        gcfg["rel-complete-graph"] = 1.0
        gcfg["rel-inference-graph-static"] = (float(gcfg["inference-edges-static"]) / gcfg["abb-count"] ** 2)
        gcfg["rel-inference-graph-dynamic"] = (float(gcfg["inference-edges-dynamic"]) /  gcfg["abb-count"] ** 2)
        gcfg["rel-sse-graph"] = (float(gcfg["sse-edges"]) / gcfg["abb-count"] ** 2)

        assert gcfg["rel-inference-graph-dynamic"] <= gcfg["rel-inference-graph-static"]
        gcfg["sse"] = sse
        gcfg["ssf"] = ssf

        S(["sse", "copied-system-states"], sse["copied-system-states"])
        S(["sse", "run-time"], sse["run-time"])
        x = sse["system-states"]
        if type(x) == list:
            S(["sse", "system-states"], sse["system-states"][0])
        else:
            S(["sse", "system-states"], sse["system-states"])

        S(["ssf", "copied-system-states"], ssf["copied-system-states"])
        S(["ssf", "run-time"], ssf["run-time"])

        S(["sse", "edges"], gcfg["sse-edges"])
        S(["ssf", "edges"], gcfg["ssf-edges"])
        S(["sse", "precision","mean"], numpy.mean(sse["precision"]))
        S(["ssf", "precision","mean"], numpy.mean(ssf["precision"]))
        S(["sse", "precision","std"], numpy.std(sse["precision"]))
        S(["ssf", "precision","std"], numpy.std(ssf["precision"]))

        S(["gcfg", "subtasks"], gcfg["subtask-count"])
        for i in ("rel-complete-graph", "rel-inference-graph-static", "rel-inference-graph-dynamic", "rel-sse-graph"):
            S(["gcfg", i], gcfg[i])
        S(["gcfg", "isrs"], gcfg["isr-count"])
        S(["gcfg", "abbs"], gcfg["abb-count"])

        self.matrix[(gcfg["subtask-count"], gcfg["isr-count"])].append(gcfg)
        self.gcfgs.append(gcfg)

    def run(self):
        # Load the statistics module, that was imported from coredos
        # to read the statstics
        statistics = imp.load_source("statistics", self.gather.stats.path)

        self.matrix = collections.defaultdict(list)
        self.gcfgs = []
        for tmp in self.gather.dicts:
            name = tmp.basename
            stats = statistics.Statistics.load(tmp.path)
            self.work_on_benchmark(name, stats)


        # Generate sorted lists
        testcases = {}
        for s in [1,2,3,4,5]:
            for i in [0, 1, 2]:
                l = []
                for gcfg in self.matrix[(s,i)]:
                    if "bench" in gcfg["name"] or "lukas_dispatch" in gcfg["name"]:
                        continue
                    testcases[gcfg["name"]] = gcfg
                    l.append(gcfg)
                l = sorted(l, key = lambda x: x["abb-count"])
                self.key(["testsuite", "matrix", "%s/%s" %(s,i)], 
                         ", ".join([self.fmtn(x["name"]) for x in l]))


        self.key(["testsuite", "count"], len(testcases))

        def output(l, key):
            if key in l[0]:
                l = sorted(l, key = lambda x: x[key])
            else:
                l = sorted(l, key = lambda x: x["sse"][key])

            self.key(["testsuite", "sorted-by", key], 
                     ", ".join([self.fmtn(x["name"]) for x in l]))

            by_isr_0 = [self.fmtn(x["name"]) for x in l if x["isr-count"] == 0]
            by_isr_1 = [self.fmtn(x["name"]) for x in l if x["isr-count"] == 1]
            by_isr_2 = [self.fmtn(x["name"]) for x in l if x["isr-count"] == 2]

            self.key(["testsuite", "sorted-by", key, "0 isr"], 
                     ", ".join(by_isr_0))

            self.key(["testsuite", "sorted-by", key, "1 isr"], 
                     ", ".join(by_isr_1))

            self.key(["testsuite", "sorted-by", key, "2 isr"], 
                     ", ".join(by_isr_2))

            self.key(["testsuite", "sorted-by", key, "* isr"], 
                     ", ".join(by_isr_0 + by_isr_1 + by_isr_2))


        output(testcases.values(), "precision")
        output(testcases.values(), "abb-count")
        output(testcases.values(), "copied-system-states")
        output(testcases.values(), "sse-edges")

        def max_min(l, key):
            self.key(["test-suite", key, "max"], max([gcfg[key] for gcfg in l]))
            self.key(["test-suite", key, "min"], min([gcfg[key] for gcfg in l]))

        max_min(testcases.values(), "rel-complete-graph")
        max_min(testcases.values(), "rel-inference-graph-static")
        max_min(testcases.values(), "rel-inference-graph-dynamic")
        max_min(testcases.values(), "rel-sse-graph")


def sliding_window(sequence, width):
    # Sort by first component
    sequence = list(sorted(sequence, key = lambda x: x[0]))
    minimum = 0.0
    maximum = 1.0

    xpos = 0.0
    new_sequence = []
    while xpos <= maximum:
        xpos += width
        xmin, xmax = xpos-width/2.0, xpos + width/2.0
        values = [x[1] for x in sequence if xmin < x[0] and x[0] <= xmax]
        if len(values) == 0:
            continue
        new_sequence.append((xpos, numpy.mean(values)))

    return new_sequence
    
class DOSEKStatistic(Experiment):
    inputs = {"gathers": List(GatherStatsDict, []) }
    outputs = {"dref" : DatarefDict() }

    def fmtn(self,name):
        return name.replace("fail-bcc1_", "").replace("_", "-")

    def key(self, keys, value):
        self.dref["/".join(keys)] = value

    def work_on_variant(self, variant_name, stats):
        abbs = []
        for name, stat in stats.items():
            subtasks = stat.find_all("Subtask").values()
            subtask_count =  len([x for x in subtasks if not "Idle" in x["_name"] and not x["is_isr"]])
            for abb in stat.find_all("AtomicBasicBlock").values():
                if not "/" in abb["_name"]:
                    continue
                # Ugly workaround to get the system call type
                abb["syscall"] = abb["_name"].split("/")[1]
                if not "sse-precision" in abb:
                    subtask = stat.get_parent(abb)
                    if "is_isr" in subtask:
                        if subtask["is_isr"]:
                            abb["syscall"] = abb["syscall"] + "inISR"
                        else:
                            # Here we got the abbs that were never called, therefore they have no
                            # activations
                            assert len(abb["activations"]) == 0
                            continue
                    else:
                        # Here are idle thread and alarm callbacks
                        assert "Idle" in subtask["_name"] or "IncrementCounter" in abb["_name"]
                        continue

                abb["subtask-count"] = subtask_count
                abbs.append(abb)

        def write_sequence(label, sequence):
            if len(sequence) == 0:
                return
            self.key(label, " ".join([("(%f, %f)" % x) for x in sequence]))

            avg = sliding_window(sequence, 0.02)
            self.key(label + ["window"], " ".join([("(%f, %f)" % x) for x in avg]))


        def write_average(label, sequence):
            self.key(label + ["mean"], numpy.mean(sequence))
            self.key(label + ["std"], numpy.std(sequence))
            self.key(label + ["count"], len(sequence))


        kernel_cycles = []
        for syscall in ["ActivateTask", "TerminateTask", "ChainTask", "GetResource", "ReleaseResource",
                        "SetRelAlarm"]:
            codesize = []
            runtime = []
            all_cycles  = []
            for abb in abbs:
                if not abb["syscall"] == syscall:
                    continue
                codesize.append((abb["sse-precision"], abb["generated-codesize"]))

                cycles = [x["cycles"] for x in abb["activations"]]
                if len(cycles) > 0:
                    runtime.append((abb["sse-precision"], numpy.mean(cycles)))
                    all_cycles += cycles

            write_sequence(["codesize", "by-precision", variant_name, syscall], codesize)
            write_average(["codesize",  variant_name, syscall], [x[1] for x in codesize])
            write_sequence(["runtime", "by-precision", variant_name, syscall], runtime)
            write_average(["runtime",  variant_name, syscall], all_cycles)
            kernel_cycles += all_cycles


        codesize = [abb["generated-codesize"] for abb in abbs if "generated-codesize" in abb]
        self.key(["codesize", variant_name, "mean"], numpy.mean(codesize))
        self.key(["codesize", variant_name, "std"], numpy.std(codesize))
        self.key(["runtime", variant_name, "mean"], numpy.mean(kernel_cycles))
        self.key(["runtime", variant_name, "std"], numpy.std(kernel_cycles))

        return len(abbs)

    def run(self):
        # Load the statistics module, that was imported from coredos
        # to read the statstics
        statistics = imp.load_source("statistics", self.gathers[0].stats.path)

        self.matrix = collections.defaultdict(list)
        self.stats = []
        for gather in self.gathers:
            gather.metadata
            name="base"
            if "mpu" in gather.metadata and gather.metadata["mpu"]:
                name += "+mpu"
            if gather.metadata["encode-system"]:
                name += "+enc"
            if gather.metadata["specialize-system"]:
                name += "+opt"
            if gather.metadata["insert-state-asserts"]:
                name += "+ass"
            if gather.metadata["insert-cfg-regions"]:
                name += "+flow"

            stats = {}
            for var in gather.dicts:
                if not "fail" in var.basename:
                    continue
                var_name = self.fmtn(var.basename)
                stat = statistics.Statistics.load(var.path)
                stats[var_name] = stat
            abb_count = self.work_on_variant(name, stats)
            print os.path.basename(gather.path), name, abb_count


if __name__ == "__main__":
    import shutil, sys
    if len(sys.argv) > 1:
        action = sys.argv[1]; del sys.argv[1]
    actions = {"gather": GatherStatsDict,
               "combine": CombineStatsDict,
               "dosek-stats": DOSEKStatistic}

    if action in actions:
        experiment = actions[action]()
        dirname = experiment(sys.argv)
    else:
        print "No action to be taken, use: %s" % ", ".join(actions.keys())

