# Set the length of IP intervals all to 1, since the Instruction
# pointer cannot be != 1 This is caused by the fact that the interval
# is != 1 if the cpu was halted, but the ip is newly set after an
# interrupt occurs.
#UPDATE trace
#       SET time1 = time2 
#       WHERE variant_id IN (SELECT id FROM variant WHERE benchmark = 'ip') 
#             AND time2 != time1;

DELIMITER $$
DROP FUNCTION IF EXISTS ec_class$$
CREATE FUNCTION ec_class(t1 bigint, t2 bigint) RETURNS bigint
BEGIN
       RETURN t2 - t1 + 1;
END$$

DROP FUNCTION IF EXISTS summed_time_to_crash$$
CREATE FUNCTION summed_time_to_crash(t1 bigint, t2 bigint, tc bigint) RETURNS bigint
BEGIN
       DECLARE t2_t1 INT default 0;
       DECLARE tc_t1 INT default 0;

       SELECT ec_class(t1, t2) INTO t2_t1;
       SELECT ec_class(t1, tc) INTO tc_t1;
       RETURN tc_t1 * t2_t1 - t2_t1 * (t2_t1 - 1) / 2;
END$$

DELIMITER ;

select variant, 'all', 'time to crash', sum(summed_time_to_crash(t.time1, t.time2, r.time_crash)) / sum(ec_class(t.time1, t.time2))
       FROM variant v 
       JOIN trace t ON v.id = t.variant_id 
       JOIN fsppilot p ON p.variant_id = t.variant_id AND p.instr2 = t.instr2 AND p.data_address = t.data_address
       JOIN result_CoredTesterProtoMsg r ON r.pilot_id = p.id
       WHERE NOT r.resulttype in ('OK', 'SDC_WRONG_RESULT', 'NOINJECTION', 'UNKNOWN', 'ERR_TIMEOUT')
             AND (r.time_crash - t.time2) < 2500000
       GROUP BY variant;


select variant, 'all', resulttype, sum(t.time2 - t.time1 + 1)
    FROM variant v 
    JOIN trace t ON v.id = t.variant_id 
    JOIN fspgroup g ON g.variant_id = t.variant_id AND g.instr2 = t.instr2 AND g.data_address = t.data_address
    JOIN result_CoredTesterProtoMsg r ON r.pilot_id = g.pilot_id 
    JOIN fsppilot p ON r.pilot_id = p.id 
    GROUP BY variant, resulttype
    ORDER BY variant, sum(t.time2-t.time1+1);

select variant, benchmark, resulttype, sum(t.time2 - t.time1 + 1)
    FROM variant v 
    JOIN trace t ON v.id = t.variant_id 
    JOIN fspgroup g ON g.variant_id = t.variant_id AND g.instr2 = t.instr2 AND g.data_address = t.data_address
    JOIN result_CoredTesterProtoMsg r ON r.pilot_id = g.pilot_id 
    JOIN fsppilot p ON r.pilot_id = p.id 
    GROUP BY v.id, resulttype
    ORDER BY variant, sum(t.time2-t.time1+1);


select "dosek/fault space", "after pruning", count(*) from trace; 

select "dosek/fault space", "before pruning", sum(time2 - time1 +1) from trace;

select variant, benchmark, 'sdc per ip bit', (t.data_address-256) * 8 + r.bitoffset,  sum(t.time2 - t.time1 + 1)
       FROM variant v 
       JOIN trace t ON v.id = t.variant_id 
       JOIN fspgroup g ON g.variant_id = t.variant_id AND g.instr2 = t.instr2 AND g.data_address = t.data_address
       JOIN result_CoredTesterProtoMsg r ON r.pilot_id = g.pilot_id 
       JOIN fsppilot p ON r.pilot_id = p.id 
       WHERE benchmark = "ip" and resulttype = 'SDC_WRONG_RESULT'
       GROUP BY v.id, t.data_address * 8 + bitoffset
       ORDER BY variant, t.data_address * 8 + bitoffset;



#DROP TABLE mttf;
#CREATE TABLE mttf
#select v.id, variant, benchmark, r.time_crash
#    FROM variant v 
#    JOIN trace t ON v.id = t.variant_id 
##    JOIN fspgroup g ON g.variant_id = t.variant_id AND g.instr2 = t.instr2 AND g.data_address = t.data_address
#    JOIN fsppilot p ON p.variant_id = t.variant_id AND p.instr2 = t.instr2 AND p.data_address = t.data_address
#    JOIN result_CoredTesterProtoMsg r ON r.pilot_id = p.id
#    WHERE true
#           #AND NOT(0x115200 <= injection_instr_absolute AND injection_instr_absolute < (0x115400))
#           AND 0x104006 = injection_instr_absolute AND injection_instr_absolute < (0x105000)
#
#           AND resulttype = 'OK_DETECTED_ERROR'
#           AND variant in ('dosek/base+opt+ass+flow', 'dosek/base+ass')
#           #AND benchmark = 'mem'
#    #GROUP BY variant, resulttype, 8*t.data_address+bitoffset
#    #GROUP BY v.id, resulttype
#    #HAVING sum(t.time2 - t.time1 +1)/1000000 > 1.0
#    ORDER BY v.id, resulttype
#;
#
