PROJECT=thesis

all:
	latexmk -pdflatex='pdflatex --shell-escape --synctex=-1 %O %S' -pdf $(PROJECT).tex
clean-all:
	rm -f *.dvi *.log *.bak *.aux *.bbl *.blg *.idx *.ps *.eps *.pdf *.toc *.out *~

clean:
	latexmk -C $(PROJECT).tex
	rm -f tikz/*

html: all
	for i in $$(grep standalone figs/*.tex | sed 's/:.*$$//' | sort -u); do \
		latexmk -outdir=tikz -pdflatex='pdflatex --shell-escape --synctex=-1 %O %S' -pdf $$i; \
	done; true
	cd tikz; for i in *.pdf; do pdftocairo -png $$i; done
	htlatex thesis-html "" "xhtml,charset=utf-8" -utf8 -shell-escape


