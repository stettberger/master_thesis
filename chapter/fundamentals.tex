\chapter{Fundamentals}\label{cha:fundamentals}

In this chapter, I will present the essential concepts and standards
for this thesis. They are well-known from the literature, and I will
only discuss them as detailed as it is necessary to understand this
work. For a more elaborated discussion on those topics please consult
the referenced publications. First, the \acs{OSEK} operating system
standard is revisited. OSEK defines the construction rules for the
real-time systems, that are analyzed in this work.  Afterwards I will
reexamine the concepts used by Scheler~\cite{scheler:11:phd} to
abstract and manipulate the real-time system architecture. Especially
the notion of \acp{CFG} and \acp{ABB} are of special interest for this
work.

\section{The OSEK/VDX Operating System Standard}
\label{sec:fundamentals^osek}

The OSEK/VDX\footnote{\acsu{OSEK} - \enquote{\acl{OSEK}} (open systems
  and their interfaces for electronic in automobiles)} operating
system specification was developed by the automotive industry, aiming
for an \enquote{open-ended architecture} for control units in
vehicles~\cite[p. 2]{OSEKSpec223}. This open-ended architecture allows
the easy integration of components built by different vendors into one
control unit. The \ac{OSEK} specification defines not only an API
(OSEK-OS) for a \emph{single-core} \ac{RTOS}, but also a inter-process
communication interface (OSEK-COM~\cite{OSEKComSpec303}) and network
management for a distributed system (OSEK-NM~\cite{OSEKNMSpec253}). In
addition to the event-triggered OSEK-OS, also an additional
specification for a time-triggered real-time operating-system, that
runs on top of OSEK-OS is defined
(OSEK-TIME~\cite{OSEKtimeSpec10}). For this work, only the basic
operating system specification OSEK-OS is of interest, and I will
refer to it from now on only as OSEK.

The main concept of \ac{OSEK} is the notion of a task. A task is one
flow of execution that is managed by the operating system. At
various points the operating system can preempt the current task
execution and start or resume another task flow. Since OSEK is a
\acf{RTOS}, more stringent rules for tasks are
applied to get deterministic and predictable behavior. Each task
has a static priority and a defined entry point, which cannot be
changed at run time. Tasks are in one of three states: \RUNNING,
\READY or \SUSPENDED.

\begin{figure}[ht]
   \centering
   \inputfig{figs/OSEK-state-diagram}
   \slcaption{OSEK task state diagram (\cite[Fig.~4.3]{OSEKSpec223})}
   \label{fig:osek-task-state-transitions}
\end{figure}
 
The transition model for task states is depicted in \prettyref{fig:osek-task-state-transitions}. A
task is activated by an external event or by another task with an explicit system call. It is
started and preempted by the system scheduler and cannot be terminated by any other entity than
itself. The scheduling rules for starting and terminating tasks are very strict. The operating
system ensures that the highest-priority task, which is \READY, is always started or resumed. When
another task with a higher priority becomes \READY, the current the current (low-priority) task is
preempted and will be resumed when the higher priority task terminates. In short, at any given time
the task with the highest priority that can run, will run.

The external activation of tasks can be done by periodic or by non-periodic events; see Chapter 6 and 7
in \textcite{liu:00}. Periodic events are
implemented by alarms and counters. Counters are represented by a counter value
that is incremented by the hardware and wraps around at a preconfigured value. Each alarm is
\enquote{wired} to one of these counters. Alarms are either in one-shot mode or in recurring
mode. When the counter reaches the alarm's activation value, the is triggers and activates the
preconfigured alarm-action. Besides low-latency alarm-callbacks, the activation of a task is a
possible action.

OSEK handles non-periodic events~\cite[p. 40]{liu:00} in terms of \acp{IRQ} that trigger a
user provided \ac{ISR}. There are two different kinds of \acp{ISR}: ISR1s may not use system calls
and are very low-latency, since they do not have to be synchronized with the operating system. ISR2s
are synchronized with the kernel and can therefore use a limited set of system calls. For example,
an ISR2 can activate another task by using a system call. It is up to the operating system
developer, whether ISR2s can be preempted (like tasks) or be re-interrupted by another \ac{ISR}. As
ISR1s do not interact with the operating system, they are of no further interest for the system
analysis undertaken here and the term \ac{ISR} refers always to ISR2s.

Critical sections, which can be entered by different tasks, are managed with \ac{OSEK} resources. An
OSEK resource has two states: it is either taken or free. If a task wants to enter the critical
section, it takes the resource. After the critical section, the task gives the resource back and the
resource is free again. When using critical regions in real-time systems, uncontrolled priority
inversions have to be prevented. Image a situation where
a low-priority task is in a critical region that is also used by a high-priority task. If the low-priority
task is now preempted by a mid-priority task, the resource cannot be released. If the high-priority task
preempts the mid-priority task and requests the resource, the operating system cannot fulfill the
request since the resource is already occupied by the preempted low-priority task. The described
situation may lead to deadline-misses or dead-lock situations, like it happened on the Pathfinder
Mars Mission~\cite{jones:97:mars,Wilner:97:rtss}. 

In order to prevent priority inversion, OSEK implements a stack-based priority ceiling
protocol~\cite{sha:90:ieeetc}: Each task has a dynamic priority, which is initialized with the
static priority. When a task acquires a resource, the its dynamic priority is increased immediately
to the ceiling priority of the resource. The ceiling priority of a resource is the highest static
resource of all tasks that are allowed to acquire the resource. The priority ceiling protocol
prevents a mid-priority task from preempting the task within the critical region, since the ceiling
priority of the resource is the static priority of the high-priority task. When a task releases a
resource, the dynamic priority drops to the highest ceiling priority of all resources occupied
by the task or, if no resource is acquired, to the task's static priority. With the \ac{PCP},
deadlocks and uncontrolled priority inversion are prevented, which is highly desirable for real-time
systems.

Producer-Consumer synchronization is done with events. For events the additional task state \WAITING
is introduced. A task can wait for an event and transits into the waiting state. Waiting tasks are
not considered during scheduling, even when they have the highest priority. Every other task (or an
alarm) can release the event, which brings the waiting task back to the \READY state. The
rescheduling takes places immediately, after the event was released.

\begin{listing}[ht]
  \begin{ccode}
TASK TaskA {
  PRIORITY = 2;
  AUTOSTART = TRUE;
  RESOURCE = resource2;
}
\end{ccode}
\slcaption[Example for an OIL declaration]{TaskA is declared with static priority 2, it is
  automatically set to \READY when the operating system boots and can obtain the
  \texttt{resource2}}\label{lst:oil-example}
\end{listing}

\ac{OSEK} is designed with resource efficiency in mind. Therefore, it has some limitations unknown
from general-purpose operating systems.  The system is preconfigured before the actual run-time. In
a domain specific language, the \ac{OIL}~\cite{OIL25}, the developer declares the system objects
statically. The number of tasks and their static priorities are declared as well as alarms, the
alarm-actions, and \acp{ISR}. This system description allows static allocation of system objects and
their static configuration. It avoids dynamic memory management within the operating system, and
therefore increases the predictability of the system. \prettyref{lst:oil-example} shows an excerpt
of a system configuration written in \ac{OIL}. \taskname{TaskA} is declared with a static
priority of~2. The declared task starts automatically, when the system is booted and
\taskname{TaskA} is allowed to acquire \texttt{resource2}. From this snippet we know that
\texttt{resource2} has at least a ceiling priority of~2.

\prettyref{tab:osek-system-calls} is an incomplete overview of the OSEK system calls, which the
specification also calls system services. System calls either get system objects, like task and
alarm identifiers, or integers, like counter values, as arguments.

\begin{table}
  \centering
  \scriptsize
  \begin{tabular}{llp{7cm}}\toprule
    System Call & Arguments & Brief Description\\\midrule
    \syscall{ActivateTask} & \verb|TaskID| & 
           The task \verb|TaskID| is transferred from the \SUSPENDED state
           into the \READY state\\
    \syscall{TerminateTask} & -- & 
           This service causes the termination of the calling task. It is transferred to the
           \SUSPENDED state.\\
    \syscall{ChainTask} & \verb|TaskID| &
           Terminates the calling task, while it atomically activates the task \verb|TaskID|.\\
    \syscall{GetResource} & \verb|ResID| &
           Acquires the resource \verb|ResID|, so that it enters the associated critical region.\\
    \syscall{ReleaseResource} & \verb|ResID| &
           Leaves the critical region associated with the resource \verb|ResID|. The resource is free
           afterwards.\\
    \syscall{SetRelAlarm} & \verb|AlarmID, inc, cyc| & 
           Winds up an alarm. After \verb|inc| ticks have elapsed the alarm action is triggered. If
           \verb|cyc| is not zero, reoccurs afterwards every cycle ticks (recurring mode), otherwise the
           alarm is only in single-mode. \\
    \syscall{CancelAlarm} & AlarmID & 
           Cancels the activated alarm AlarmID.\\
    \syscall{EnableAllInterrupts} & -- &
           Disables all interrupts for which the hardware supports disabling.\\
    \syscall{EnableAllInterrupts} & -- &
           Restores the regcognition status of all interrupts that were disabled by \syscall{SuspendAllInterrupts}.\\
    \bottomrule\end{tabular}
  \slcaption[OSEK System Call Overview]{The system calls (system services) are described
    in~\cite[Chapter 13]{OSEKSpec223}.}
  \label{tab:osek-system-calls}
\end{table}

\ac{OSEK} is used on a wide range of target systems of various sizes. Hence not all features are
used by every application, \ac{OSEK} defines four conformance classes. The basic conformance
class 1 (BCC1) defines the most fundamental class. With BCC1 each priority can only contain one
task, every task can be activated only once and no events may be used (no \WAITING state). This
conformance class, with some extensions regarding to resource usage, is the base for this thesis. I
aim for this conformance class, since it already allows the design of complex systems and contains
most of the problems to be discussed. At some point, hints are given how extend this work to more
advanced conformance classes.


\section{Atomic Basic Blocks: Abstracting Real-Time Systems}
\label{sec:fundamentals^atomic-basic-blocks}
In his dissertation, Scheler~\cite{scheler:11:phd} describes real-time systems at an abstract
level. A real-time system is closely connected to its environment. This connection is expressed in
terms of measuring the physical world, computing values and using the calculated values to
manipulate the physical world. The notion of time and a timely behavior is of distinguished
importance for a real-time system. This interleaved connection of the real-time system and the
physical world calls Scheler the \enquote{external-view of a real-time
  system}~\cite[p. 21]{scheler:11:phd}. This external view is expressed in terms of events, tasks
and deadlines and has to be implemented by the real-time engineer. Scheler calls this technical
implementation, which fits the external view, the \emph{internal-view of a real-time
  system}~\cite[p. 23]{scheler:11:phd}.

The internal-view is mainly defined by the \ac{RTSA}. In industry two main \acp{RTSA}
developed~\cite[Section 3.4.1]{scheler:11:phd}\cite[p. 60f]{liu:00}: \emph{event-triggered real-time systems} and
\emph{time-triggered real-time systems}.  Event-triggered systems release jobs upon external events,
which can fire at any given time. The real-time operating system's job is to execute jobs in such a
way, that they meet their deadlines. Whether the jobs meet their deadlines in any given situation,
has to be proven by a schedulability analysis. The dynamic nature of event-triggered real-time
systems is more intuitive to developers; therefore these systems are considered easier in their
construction. But their timely behavior is harder to prove~\cite[p. 72]{liu:00}.

The other \ac{RTSA} is the time-triggered architecture. In a time-triggered system, every action is
controlled by a timetable. The entries in the timetable are executed sequentially with a fixed
time delay. A hardware timer is used to trigger the next action the timetable defines. If the last
entry is finished, the execution cycle starts again at the first table entry. Therefore these
systems are strictly periodic. This strict periodicality make time-triggered systems easier to
verify, but the construction of timing tables is a difficult and error-prone task, when done manually.

Therefore, Scheler identifies common structures, used in both paradigms, and aims for an automatic
translation of the \ac{RTSA}. This allows the real-time engineer to write an event-triggered system
and translate it automatically to a time-triggered system. The main abstraction Scheler uses is the
\emph{atomic basic block dependency graph}.

Scheler borrows concepts that are commonly used in compiler technology. Therefore it is useful to
take first a look at these concepts and to investigate on their descriptive strengths for the
\ac{RTSA} afterwards. Compilers use the representation of a program as \acf{CFG} consisting of
\acp{BB}. \textcite[p. 528]{aho:86} define a basic block like this:

\begin{definition}[Basic Block]
  A \emph{basic block} is a sequence of consecutive statements in which flow of control enters at
  the beginning and leaves at the end without halt or possibility of branching except at the end.
\end{definition}

Basic blocks are identified by their \emph{leaders}. For a function, statements are marked as
leaders, if they follow a branch statement or are target of a branch statement. The first statement
of a function is always a leader. A \ac{BB} consists of a leader and all following non-leader
statements up to, but not including, the next leader or the end of the
function~\cite[p. 529]{aho:86}.

Within a basic block, the flow of control progresses linearly. The basic blocks can be connected in
a directed graph. There, edges are drawn from a source block to a target block, iff the execution
flow \emph{can} proceed from the last instruction of the source block to the first instruction of
the target block. This directed graph is called the \acf{CFG} and it subsumes all possible execution
paths.  Normally, the \acp{CFG} are constructed for single functions, therefore they have an
\emph{entry basic block} and an \emph{exit basic block}. Whether function call statements, which
transfer the control to the \ac{CFG} of another function, are leader statements or not is
implementation specific.

For global (whole-program) optimizations an \ac{ICFG} is constructed. The \acp{CFG} of all functions
in a program are mangled into one directed graph. A basic block containing a function call is connected
to the entry block of the called function. From the exit block of the called function an edge is
drawn to the basic block containing the statement following the original call statement. This may be
the same block as the calling basic blocks or one of its direct successors~\cite{sharir:81:callstrings}.

Scheler identifies the concept of \acfp{ABB} as an abstraction for the \ac{RTSA}. \acp{ABB} are
similar to basic blocks, but have a whole-system view. In the internal-view, tasks are \emph{dependent} on each other. Dependencies can be either directed or
undirected. Directed dependencies model that some action has the be completed before another one can
start, for example one task sets another task ready. Undirected dependencies express that two actions
exclude each other on the timeline: \emph{critical sections}. To gain a fine granularity for these
dependencies, the system is divided into \acp{ABB}, which are connected in an \ac{ABB} dependency
graph. Scheler uses the following rules to identify \acp{ABB}~\cite[p. 45]{scheler:11:phd}:

\begin{enumerate}
\item \acp{ABB} include one or more basic blocks of a function, that are a connected component of
  the function's \ac{CFG}. An \ac{ABB} is a \emph{control flow region}.
\item Every \ac{ABB} has one definite entry basic block; the \ac{ABB} can only
  be entered via this block. Analogues, every \ac{ABB} has one
  definite exit basic block; the \ac{ABB} can only be left via this
  block. An \ac{ABB} is a \emph{single-entry single-exit region}.
\item \acp{ABB} reach from the end of the last \ac{ABB} to the next \ac{ABB} endpoint. Endpoints
  are sources or targets of system dependencies, for example system calls.
\item If an \ac{ABB} endpoint is contained within a basic block, then the block is split up at the ABB-endpoint.
\end{enumerate}

The main drawback of the dependency graph, discussed by Scheler, is that its construction is
flow-insensitive. The fact that the application logic may prohibit a dependency at one point is not
taken into account. For a more detailed discussion of \acp{ABB} and the \ac{ABB} dependency graph
please refer to \textcite{scheler:11:phd}. To give a practical example, I will briefly sketch the
\ac{ABB} and graph construction phases for an event-triggered real-time system, implemented with
\ac{OSEK}:

\begin{listing}[ht]
  \begin{ccode}
TASK(SerialByte) {
   unsigned char received = getByte();
   message_add(received);

   if (message_isComplete()) {
      ActivateTask(MsgHandler):
   }
   TerminateTask();
}

TASK(MsgHandler) {
   message_process();
   TerminateTask();
}
\end{ccode}
\slcaption[Example OSEK application]{The \taskname{SerialByte} task receives a byte and adds it
  to a message buffer. If the message is complete, the message processing task \taskname{MsgHandler}
  is activated.}\label{lst:example-osek-serial-byte}
\end{listing}

\prettyref{lst:example-osek-serial-byte} is the source code listing of an \ac{OSEK} system. It
resembles a simple serial-message receiver. The \taskname{SerialByte} task is activated (not
shown) when a new byte is received. The task adds the byte to a message handling subsystem. If
this subsystem signals that a message is complete, another task (\taskname{MsgHandler}) is
activated. The \taskname{MsgHandler} task processes the message and terminates itself afterwards.

The \ac{ABB} dependency graph is constructed from the task's \acp{CFG}
(\prettyref{fig:example-osek-abb-dependency^a}). The \syscall{ActivateTask} system call, which is an
ABB endpoint, is located within BB2. As the fourth rule of \ac{ABB} construction implies, this basic
block is split into two basic blocks. Each basic block is then wrapped into an \ac{ABB}
(\prettyref{fig:example-osek-abb-dependency^b}). System calls insert inter-task dependencies: ABB4
can only be executed when \taskname{MsgHandler} is activated, therefore an dependency from ABB2a is
drawn (\prettyref{fig:example-osek-abb-dependency^c}). The dependency graph can be used to translate
the event-triggered real-time system into a time-triggered one.


\begin{figure}[htpb]
\centering
   \subfloat[][Control flow graphs for the system from
       \prettyref{lst:example-osek-serial-byte}. The dashed line is the
       logical result of the \syscall{ActivateTask} system call.
       \label{fig:example-osek-abb-dependency^a}]
       {
         \inputfig{figs/RTSC-ABB-gen-a}
       }


       \subfloat[][The \ac{BB} with the system call is splitted into two \acp{ABB}. All other
                   \acp{BB} are converted only to one \ac{ABB}. \label{fig:example-osek-abb-dependency^b}]
       { \inputfig{figs/RTSC-ABB-gen-b} }

       \subfloat[][Directed edges in the resulting dependency graph express the \ac{ABB} execution
                   order. \label{fig:example-osek-abb-dependency^c}]
       { \inputfig{figs/RTSC-ABB-gen-c} }
   \caption[\ac{ABB} Dependency Graph Construction]{Construction of
     the \ac{ABB} dependency graph.}\label{fig:example-osek-abb-dependency}
\end{figure}

Another important part of this translation process is the system model introduced and used by
Scheler. The system model is a description of the external-view of the real-time system. The
real-time system engineer annotates events, deadlines and tasks. Scheler distinguishes between
tasks, that are part of the external-view, and subtasks, which are the implementation detail of the
internal-view. 

\begin{definition}[Task]
  In a real-time system, tasks contain all forms of actions to be done, when an event occurs. Tasks
  are part of the external-view of a real-time system.
\end{definition}

Each task is connected to an event, which releases it and issues a new job. The
implementation then arranges subtasks to achieve the job in the given time.

\begin{definition}[Subtask]
  Subtasks incorporates the actual event handling. They are part of the internal-view of a real-time
  system. Each task contains one or more subtasks and each task has at least one root-subtask that is
  started when the event occurs.
\end{definition}


OSEK does not use this terminology, but a different one. The OSEK term \enquote{task} maps to the
notion of a subtask, since it contains the actual event handling code. The notion of tasks is not
present in the OSEK specification. In order to not confuse you, I will use Scheler's terminology
consistently for the rest of this work.

One important constraint in Scheler's work, which is not explicitly mentioned, is that all subtasks
within a task must finish their execution, before the task's event triggers again. This limitation
is tightly coupled to the system model, since there each event is assigned a minimal inter-arrival
time and each task is annotated with a deadline.  Scheler's dissertation also contains a more
detailed discussion of the system model~\cite[p. 50]{scheler:11:phd}.

\section{Summary}\label{sec:fundamentals:summary}

The OSEK operating system standard is used to build static real-time operating systems. Many
different system objects are described in the standard. Subtask have a static priority, but can
change their dynamic priority at run-time by acquiring a resource. Using PCP, resources implement
critical sections without uncontrolled priority inversion. The scheduling is strictly priority
driven and rescheduling takes place immediately. Subtasks can be activated by other subtasks or
\acfp{ISR} with a system call. Additionally, alarms can be winded up and activate a subtask on
expiration. A consumer-producer situation can be implemented with events, which are sent by a
subtask, while another subtask waits on the event. All system objects have to be declared in the
\ac{OIL} description at compile time, and can therefore be allocated statically by an operating
system generator.

\emph{Atomic basic blocks}, presented by Scheler~\cite{scheler:11:phd}, abstract the real-time
system architecture and are used to translate an event-triggered real-time system into an
time-triggered one. \acp{ABB} are defined similar to basic blocks, which are well known from compiler
technology. \acp{ABB} are a fine-grained division of the subtask's \acf{CFG} and contain one or more
basic blocks. The \acp{ABB} can be connected in a dependency graph, that expresses forced execution
order and critical sections. The external-view of the real-time system is annotated, in terms of
tasks, events and deadlines, by the real-time engineer in the \emph{system model}.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../thesis"
%%% End:
