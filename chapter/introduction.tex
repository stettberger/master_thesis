\chapter{Introduction}
%\chapter{Einleitung}
\label{sec:introduction}

Embedding computer systems into almost every aspect of our life is one of the main characteristic of
the technological advancement at the beginning of the 21st century. General-purpose computer systems
are not only the main component of mobile systems, which get a lot of attention in the general
public, but they also control most of our industrial production lines and our automotive vehicles. Here,
special properties are required from the systems. They not only have to fulfill real-time
requirements, but they should also fit onto small and cheap devices, while being resilient to
unforeseen environmental influences. Therefore, we use specialized real-time operating systems to
orchestrate different processes within a single real-time application.

These real-time operating systems are tightly coupled to the applications they control. The software
for most real-time systems is shipped as a single software package that includes as well the
operating system as the application. This tight coupling opens a possibility for the systems
engineer: When the application on top of the operating system never changes, we can tailor the
operation as closely to the application as possible. With this tailoring process, various
non-functional properties as code size, run time, and memory consumption can be optimized to fit the
bare minimum of the application's requirements. But for this tailoring process, detailed knowledge
about the application has to be available.

In the automotive industry, a standard for operating systems emerged: the OSEK~\cite{OSEKSpec223}
standard. In the first place, OSEK is intended to ease the interaction between various component
vendors in the automotive industry. But OSEK is also a standard for \emph{static} operating
systems. In static operating systems, all kernel objects (i.e., tasks, alarms, and interrupts) have
to be declared beforehand in a system specification file. Therefore, the number and the static
characteristics of these objects is known at compile time. The application knowledge described in
the specification file can be used to drive an operating-system generator. It is best practice in
the OSEK world to allocate and initialize the system objects statically into arrays and to link a
generic OSEK implementation against it.

The application knowledge in the specification file is very coarse grained. Only the existence of
tasks and some static configuration data, like static priority and preemptability of the task, are
denoted there. Therefore, the tailoring process has to remain on that level of
abstraction. \textcite{lohmann:09:usenix} showed with their OSEK implementation CiAO the potential
of static tailoring in the OSEK world due to manual configuration. They tailored the operating
system as closely to the static requirements of the application as possible. The CiAO
operating-system family exposes configuration switches for interrupt synchronization, memory
protection, a highly modular IP stack~\cite{borchert:12:mobisys}, and many more. But all those
switches turn on or off whole components or aspects of components for the whole application. So the
tailoring is close to the application as a whole, but it is not close to distinct points within the
application.

This thesis wants to extend the amount of in-depth application knowledge that can be used in the
system-tailoring process. Not only the superficial application knowledge that is denoted in the
specification file of an OSEK system should be used, but also the interaction between the
application logic and the operating system is to be taken into account. This interaction is analyzed
across kernel activation borders and the control-flow graph of all tasks in the system is
combined in a \emph{global control-flow graph}.

The in-depth application knowledge can be used to optimize the whole real-time system towards
different non functional properties. One non functional property, which gained more and more
attention in the recent years, is the resilience against soft errors. The developments in the
fabrication of processors, like shrinking structure sizes and lower operating voltages, did not only
enable the design of complex real-time systems with high computation requirements, but made the
hardware more susceptible to transient hardware
faults~\cite{constantinescu:03:micro,borkar:2005:ieeemicro}. Single bits in memory or the processor registers
are more likely to be flipped by a cosmic particle or radiation. These soft errors can be mitigated
by expensive hardware measures, like lock-step processors or ECC-checked
memory~\cite{baleani:03:cases,yeh:96:aac}, but also software-based
measures~\cite{borchert:13:dsn,fetzer:09:safecomp} can be applied. With the in-depth application
knowledge at hand, various software-based dependability aspects, which exploit the application's
structure, can be introduced into the real-time system.

The focus of this thesis is the real-time operating system. Many software-based measures to
mitigating the effect of soft-errors demand a reliable computing base~\cite{engel:12:sobres} in form
of an operating system that either detects a soft error or makes it a benign fault, but it should
never fail silently. Since prior work showed that the static design of OSEK inherits a lower
vulnerability towards soft-errors~\cite{hoffmann:14:isorc}, the operating system standard is a good
starting point for constructing this reliable computing base. With wise design decisions and
arithmetic encoding, the \dosek operating system~\cite{hoffmann:13:prdc-fast,lukas:14:master}
already provides a four orders of magnitude lower silent-data-corruption rate than a off-the-shelve
OSEK. But this resilience has a high cost in terms of code size and run-time overhead. But not only
can the in-depth application knowledge be used to ease this overhead, but it can also be utilized to
apply further software-based measures.

The rest of this thesis is structured as following: In \prettyref{cha:fundamentals} the fundamental
methods and the prior work are presented; \prettyref{cha:system-analysis} explains the methods that
are used for gathering the application knowledge in depth; \prettyref{cha:system-construction} shows
three different methods how the knowledge can be exploited to optimize different non functional
properties of the system; The evaluation for different benchmarks scenarios is done in
\prettyref{cha:evaluation}. The thesis closes with a wider overview of the related work
(\prettyref{cha:related-work}), a brief discussion of possible future work
(\prettyref{cha:future-work}), and the conclusion (\prettyref{cha:conclusion}).

%\begin{itemize}
%  \item \textbf{Context:} make sure to link where your work fits in
%  \item \textbf{Problem:} gap in knowledge, too expensive, too slow, a deficiency, superseded technology
%  \item \textbf{Strategy:} the way you will address the problem 
%\end{itemize}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../thesis"
%%% End: 
