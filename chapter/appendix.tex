\pagestyle{plain}
\begin{appendices}

\chapter{Fault-Injection Campaign}\label{sec:appendix:fault-inject-camp}

Here, I want to present you some of the graphs that were removed from the main corpus of this
thesis, not because they are not interesting or informative, but because most of their information
is partly redundant with other graphs and does not provide new insights. 

\begin{figure}[thb]
  \centering
  \def\variant{base}
  \def\coords{1/, 2/+flow, 3/+ass, 4/+opt}
  \pgfplotsset{xticklabels={{Baseline},{+flow},{+ass},{+opt}}}
  \inputfig{figs/template-sdc-locations-log}

  \slcaption[SDC Rates for the \emph{unencoded} \dosek without Spatial Isolation]{}
\end{figure}

\begin{figure}[thb]
  \centering
  \def\variant{base+enc}
  \def\coords{1/, 2/+flow, 3/+ass, 4/+opt}
  \pgfplotsset{xticklabels={{Baseline},{+flow},{+ass},{+opt}}}
  \inputfig{figs/template-sdc-locations}

  \slcaption[SDC Rates for the \emph{encoded} \dosek without Spatial Isolation]{}
\end{figure}

\begin{figure}
  \def\varianta{base+mpu+ass}
  \def\variantb{base+mpu+opt+ass}
  \def\mylegend{+ass,+opt+ass}
  \inputfig{figs/template-sdc-per-bit.tex}

  \slcaption[SDC Distribution for the Instruction Pointer for the unencoded \dosek]{When injecting
    faults into the instruction pointer, the system-call tailored variant shows a higher SDC rate for
    low bit offsets. Low bit offsets equal short jumps by a power of two.}
  \label{fig:sdc-distribution-ip-base}
\end{figure}

\begin{figure}
  \def\varianta{base+mpu+enc+ass}
  \def\variantb{base+mpu+enc+opt+ass}
  \def\mylegend{+ass,+opt+ass}
  \inputfig{figs/template-sdc-per-bit.tex}

  \slcaption[SDC Distribution for the Instruction Pointer for the encoded \dosek]{When injecting
    faults into the instruction pointer, the system-call tailored variant shows a higher SDC rate for
    low bit offsets. Low bit offsets equal short jumps by a power of two.}
  \label{fig:sdc-distribution-ip-base+enc}
\end{figure}


\chapter{Reproducibility}

The results presented in \prettyref{cha:evaluation} were generated almost entirely by an automatic
process. This automation allows not only an easy access to the raw data, which was used in the
evaluation, but makes the results also more easy to reproduce. In this appendix, I want to describe
the raw data, which can be obtained together with the \LaTeX{} source of this document, and the formal
description of the experiment scripts.

The experiment descriptions were written in the
versuchung\footnote{\url{https://www.github.com/stettberger/versuchung}} framework. This framework
allows the definition of formal experiment descriptions as Python code. Each experiment takes
well-defined input parameters, instrument \dosek and \fail to run with the given arguments, and
output well-defined outputs. These outputs can be post-processed by other versuchung
experiments. \prettyref{fig:versuchung-flow} show the data flow within those versuchung experiments
for each of the three evaluation sections.

For each evaluation section, the experiments output a \texttt{data.tex} file. These files contain
dataref\footnote{\url{https://www.github.com/stettberger/dataref}} keys. dataref is a \LaTeX{}
package that allows the user to define symbolic data points and to reference them from within the
document. A data point's value can be printed directly, it can be used within a calculation, or it
can be used as an input parameter to a
PGFPlot\footnote{\url{https://www.ctan.org/pkg/pgfplots}}. By using these techniques, all
numbers, percentages and graphs are always up-to-date with the latest run of the experiment results.

The \texttt{GatherStatsDict} (see \prettyref{fig:versuchung-flow}) experiment checks out a \dosek
source tree. It configures \dosek with the given configuration vector and builds the \dosek test
suite and the \copter benchmark. For the \prettyref{sec:evaluation^system-construction}, the
experiment is furthermore instructed to record a golden run of the application.

The \texttt{CombineStatsDict} calculates the dataref keys, related to the code size used by the
application. Furthermore the system state precision and other metrics for the analyses are extracted
from the reports that are generated during the construction of \dosek.

The \texttt{DOSEKStatistic} experiment analyses the golden-run traces for each variant and
calculates average activation times and other metrics related to the dynamic behavior of the \dosek
applications.

The \texttt{FailTrace} experiment builds the \copter benchmark for various configurations. As a
result, 16 golden-run traces and the corresponding system images are stored. These traces are
imported into a MySQL database by the \texttt{FailImport} experiment.

The activation of \fail is the only step of the evaluation that was not codified with versuchung,
since the computer cluster of the University of
Erlangen-Nuremberg\footnote{\url{https://www.rrze.uni-erlangen.de/dienste/arbeiten-rechnen/hpc/systeme/}}
was utilized. Operating this cluster required some manual worksteps that were not worth
automating. \fail writes back the result of the fault-injection campaign into the database. The
results are  queried by a SQL script and the output is only transformed into dataref keys, but not
further modified.

\begin{figure}\scriptsize
  \centering
  \def\experiment#1#2{%
      \tikz\node[minimum height=0.8cm]{#1};
      \tikz\node[minimum height=0.8cm]{\attachfile[icon=Tag]{#2}};
  }
  \tikzstyle{experiment}=[draw,align=left,fill=blue!40,draw=blue!60,thick]
  \tikzstyle{parameter}=[draw,align=left,fill=red!40,draw=red!60,thick]
  \tikzstyle{result}=[draw,align=left,fill=yellow!40,draw=yellow!60,thick]

  \begin{tikzpicture}

    \node[parameter] (dosek1)  {\dosek Source Code\\Repository\\\texttt{456cabcb17556}};
    \node[parameter,below=1 of dosek1] (config1) {Base\\Configuration};


    \node[experiment,right=1 of dosek1,yshift=-1cm] (gather1)
        {\experiment{GatherStatsDict}{rawdata/gather-stats-dict.py}};
    \node[experiment,right=2 of gather1] (combine1)
        {\experiment{CombineStatsDict}{rawdata/gather-stats-dict.py}};
    \node[result,below=1 of combine1] (result1) 
        {\experiment{data.tex}{rawdata/analysis-quality/CombineStatsDict/data.tex}};

    \draw[->] (dosek1) -|- (gather1.175);
    \draw[->] (config1) -|- (gather1.185);
    \draw[->] (gather1) -- node[midway,above] {\dref{/stats.dict/testsuite/count} reports}  (combine1);
    \draw[->] (combine1) -- (result1);

    \node[draw, inner sep=0.4cm,minimum width=\linewidth,
          label={above:\prettyref{sec:evaluation^system-analysis} --- \texttt{rawdata/analysis-result/}}, 
          fit=(dosek1) (config1) (gather1) (combine1) (result1)] (box1) {};
  \end{tikzpicture}

  \qquad

  \begin{tikzpicture}

    \node[parameter] (dosek1)  {\dosek Source Code\\Repository\\\texttt{052df242c6ac}};
    \node[parameter,below=1 of dosek1,label=above:10 configs] (config1) {
      \begin{minipage}{2.7cm}\centering
        \begin{tabular}{ll}\toprule
          -enc & +enc \\\midrule
          Base & Base \\ 
          +ass & +ass \\
          +opt & +opt \\
          +flow & +flow \\
          +all & +all \\\bottomrule
        \end{tabular}
      \end{minipage}};

    \node[experiment,right=1 of dosek1,yshift=-1cm] (gather1)
        {\experiment{GatherStatsDict}{rawdata/gather-stats-dict.py}};

    \node[experiment,right=2 of gather1] (combine1)
        {\experiment{DOSEKStatistic}{rawdata/gather-stats-dict.py}};

    \node[experiment,below=0.5 of gather1,opacity=0.5] (gather2)
        {\experiment{GatherStatsDict}{rawdata/gather-stats-dict.py}};
    \node[result,below=1 of combine1] (result1) 
        {\experiment{data.tex}{rawdata/dosek-statistic/DOSEKStatistic/data.tex}};

    \draw[->] (gather1) -- node[midway,above] {520 reports}  (combine1);
    \draw[->] (combine1) -- (result1);

    \draw[->] (dosek1) -|- (gather1.175);
    \draw[->] (config1) -|- (gather1.185);

    \node[draw, inner sep=0.4cm,minimum width=\linewidth,
          label={above:\prettyref{sec:evaluation^system-construction} --- \texttt{rawdata/dosek-statistic/}}, 
          fit=(dosek1) (config1) (gather1) (combine1) (result1)] (box1) {};
  \end{tikzpicture}

  \qquad

  \begin{tikzpicture}

    \node[parameter] (dosek1)  {\dosek Source Code\\Repository\\\texttt{052df242c6ac}\\\texttt{b563ced9d6}};
    \node[parameter,below=1 of dosek1,label=above:16 configs] (config1) {
      \begin{minipage}{2.7cm}\centering
        \begin{tabular}{ll}\toprule
          -enc & +enc \\\midrule
          Base & Base \\ 
          +ass & +ass \\
          +opt & +opt \\
          +flow & +flow \\\midrule
          \multicolumn{2}{c}{+ combinations}\\\bottomrule
        \end{tabular}
      \end{minipage}};

    \node[experiment,right=1 of dosek1,yshift=-1cm] (gather1)
        {\experiment{FailTrace}{rawdata/fail-experiment.py}};
    \node[experiment,right=3 of gather1] (combine1)
        {\experiment{FailImport}{rawdata/fail-experiment.py}};

    \node [below=1 of  combine1, database,align=left,pin={[align=left]45:Dump\\Available}] (mysql) {MySQL};

    \draw[->] (gather1) -- node[midway,above] {16 golden runs}   
                node[midway,below,align=left] {execution traces\\\dosek ELF Files\\Bootable ISOs} 
                (combine1);
    \draw[->] (combine1) -- (mysql);

    \draw[->] (dosek1) -|- (gather1.175);
    \draw[->] (config1) -|- (gather1.185);

    \node[left=2 of mysql, experiment] (fail) {\experiment{\fail}{rawdata/fail-experiment.cc}};
    \draw[->,bend right] ($(mysql.west)+(up:1mm)$) to ($(up:1mm)+(fail.east)$);
    \draw[->,bend right] ($(fail.east) +(down:1mm)$) to ($(down:1mm)+(mysql.west)$);


    \node[experiment,below=1 of mysql] (sql) {\experiment{SQL
        Query}{rawdata/fault-injection/result.sql}};
    \draw[->] (mysql) -- (sql);

    \node[result,below=1 of sql] (result1) 
        {\experiment{data.tex}{rawdata/fault-injection/data.tex}};
    \draw[->] (sql) -- (result1);


    \node[draw, inner sep=0.4cm,minimum width=\linewidth,
          label={above:\prettyref{sec:evaluation^fault-inject-camp} --- \texttt{rawdata/fault-injection/}}, 
          fit=(dosek1) (config1) (gather1) (combine1) (result1)] (box1) {};
  \end{tikzpicture}

  \slcaption[Experiment Flow Chart]{For each evaluation chapter, a separate experiment workflow was
    used. In the PDF version the Icons link to the experiment descriptions}\label{fig:versuchung-flow}
  
\end{figure}

%\FIXME{I want to do this}
\end{appendices}
%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../thesis"
%%% End: 
