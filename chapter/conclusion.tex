\chapter{Related Work}\label{cha:related-work}

In this chapter, I want to present previous work that was not the fundamental of this thesis, but is
related to the various topic discussed in this work.

A global view on the interaction between operating system and application was already proposed by
\textcite{bertran:06:wiosca}. There a global control-flow graph for a complex embedded system, which
was built on top of Linux, was constructed in a flow-insensitive manner. System-call entry points
and library entry points were connected to the corresponding call sites. On this GCFG, dead code
elimination in terms of removing uncalled system-calls and unreferenced library functions resulted
in a reduced code size of the system image. In contrast to this work the scheduling and system-call
semantic was not taken as closely into account as I did it. This stems mainly from the fact that the
Linux kernel has not such a strict and simple semantic as OSEK.

\textcite{barthelmann:02:lctes} used the system description file of an OSEK system and the included
static priorities to determine basic-block transitions that are impossible. With this application
knowledge, the number of registers that has to be saved in the preemption of a task is reduced by
the compiler. Here, also application knowledge is used to influence a non functional property of an
OSEK kernel. But in contrast to my thesis, the scheduling semantic of OSEK was used in a
flow-insensitive manner.


% \item {Global Control Flow Graph
%     \begin{itemize}
%     \item Bertran: Building a Global System View for Optimization Purposes
%     \item Barhelmann: Inter-Task Register-Allocation for Static Operating Systems
%     \end{itemize}
% }

In the area of formal methods and verification, the OSEK semantic got some attention:
\textcite{waszniowski:08:rts} designed a model for the OSEK
standard for the model checker UPPAAL\footnote{http://www.uppaal.org/}. They modeled all components
as timed automata and took also inter-process communication (OSEK events) into account. Their main focus
was verifying application properties, like schedulability analysis. \textcite{huang:11:tase} modeled
OSEK as communicating sequential processes (CSP). The application subtasks were modeled without
considering the internal application structure and interrupts were excluded. With this model, they
could verify different properties of their OSEK system, like dead-lock freedom and freedom of
priority inversion. For my approach, these models could provide a more formal definition of
\function{system\_semantic}.

% \item {Formal Methods and Verification
%     \begin{itemize}
%     \item Huang: Modeling and Verifying the Code-Level OSEK/VDX Operating System with CSP
%     \item Waszniowski: Analysis of OSEK/VDX based Automotive Applications
%     \item Waszniowski: Formal Verification of OSEK/VDX Based Applications
%     \end{itemize}
% }

% \item {System Call Specialization
%     \begin{itemize}
%     \item Synthesis and descendants by Pu et al.
%     \end{itemize}
% }

System specialization was already discussed by the operating-system community for general-purpose
operating systems. \textcite{pu:88:computingsystems} developed the Synthesis kernel, which included
a code synthesizer that produced optimized code paths at run time for often invoked system calls,
like \function{read}. Due to manual implementation of code templates, which are then filled by the
synthesizer, huge performance benefits arouse from shorter kernel execution paths. In comparison to
the dynamic Synthesis system, my approach toward system-call specialization took also the in-depth
application knowledge into account and is executed off-line. Pu et al. also mention the problem of
code-size explosion. \textcite{mcnamee:01:sttt} used Tempo, a partial evaluator for C programs, and
a set of specialization predicates to identify functions automatically for specialization within the
kernel. Here, the specialization was done also dynamically at run time and the specialization does
not include detailed application knowledge.

% System State Asserts

% Control Flow Monitoring
Several approaches towards control-flow monitoring were developed for application
logic. \textcite{benso:11:ats} uses regular-expression automata to check whether the executed basic
block sequence of an application is correct. \textcite{oh:02:journal} presented an approach with
\emph{software signatures}. Each basic block is assigned an unique number; when the basic block is
entered or left, a global variable is xored with the unique number. Without control-flow errors, the
global variable contains always exactly the unique number of the currently executed basic
block. \textcite{yau:80:tse} divided the control-flow graph into loop-free regions. For each region
a database of possible paths is encoded and checked during the execution. All mentioned approaches
do only consider the control-flow graph of a single function or subtask, but could be extended to
catch control-flow errors on the ABB and GCFG level.

\chapter{Future Work}\label{cha:future-work}

In this chapter, I want to present a few ideas that came to my mind during the development and
writing phase of this thesis. These ideas are the result of many discussions with my supervisors
Martin Hoffmann and Daniel Lohmann.

\section{Usage in Other Whole-System Analyzes}

The GCFG provides a global view upon the interaction between the application and the real-time
system. This knowledge could be exploited also in other areas of the real-time world. 

One example of such an area is the analysis of the worst-case execution time. When doing flow-sensitive
WCET analysis, the micro-architecture and the cache state are important factors to the precision of
the WCET over-approximation. When not taking the operating system into account, the WCET analyzer
has to assume after each system call that the cache does not contain any data of the subtask's
working set, since all cache lines could be extruded by another subtask. \textcite{chong:13:rtss}
proposed an approach to incorporate the influence of the operating system to ease the effect on the
system state during the WCET analysis. They annotated each system call (type) with a maximal effect
that can occur onto the system state. By this flow-insensitive approach, the influence of system
calls is limited. With the GCFG analysis at hand, the WCET analysis could be provided by even better
annotations on the influence of the operating system.

Similar to the WCET analysis, could a whole-system application compiler, like
KESO~\cite{erhardt:11:jtres,wawersich:07:ifipesd}, benefit from the global view upon the system's
control flow. With the information which basic block is executed on the machine directly after a
system call, constant propagation across kernel barriers is achievable. Also could the global view
and a detailed static analysis of the application code reveal protocol violations in the application
code. Static code checkers could be made operating-system aware in a flow-sensitive manner.

\section{A Better Control-Flow Monitoring}\label{sec:better-control-flow}

In \prettyref{sec:contr-flow-monit}, I presented an approach to insert control-flow monitoring
checks into the system calls for a positive test. The positive test can check whether the execution
flow jumps into the flow region without entering it via the region leader. Here I want to present an
approach, which was not implemented by me because of technical difficulties. It uses the concept of
the \emph{dominance frontier}~\cite{cytron:91:toplas} to determine all resetting points for the
marker when the region is left.


\begin{definition}[Dominance frontier]
  The dominance frontier of a CFG node X is the set of all nodes Y, such that X dominates a
  predecessor of Y, but does not strictly dominate Y.
\end{definition}

So the dominance frontier of a region leader are those ABBs that are executed just after the region
is left. If we reset the region marker there, the resetted marker also indicates an information: We
have left the region on a valid path. Therefore, we can add checks outside the region that the
regions markers are not present.  Resetting could also be done in the \enquote{last} dominated block
within the region, which are the predecessors of the dominance frontier (see
\prettyref{fig:exploitation^dominator-frontier}).

\begin{figure}[thb]
  \centering
  %\includegraphics[width=0.5\textwidth,page=1,angle=270]{scans/2014-05-09-1.pdf}

  \inputfig{figs/dominance-border}

  \caption[Dominance Borders and Resetting points]{
    A control flow region with its leader and possible resetting
    points for the region marker, which can be located in the
    dominance frontier or directly \enquote{before}.
  }\label{fig:exploitation^dominator-frontier}
\end{figure}

But when only the system-call blocks are manipulated by the OS generator a problem arises. For having
a sound check for being outside the region, we must ensure that no control flow \enquote{sneaks}
around the resetting nodes. If as well the node within the dominance frontier and its
predecessor(s) in the region are computation blocks, the OS generator has no chance to insert the
resetting operation. So if the control flow leaves the region through that path, the marker is not
reset and the next negative check would cause an false positive. \abb{2} and \abb{6} from
\prettyref{fig:exploitation^dominator-frontier} are an example of such a bypass. This could be
solved by manipulating computation blocks, but because of technical difficulties this was not
implemented for this work. Nevertheless, with a solution here, a negative check could be implemented
as cheaply as the positive check.

\section{Converting OSEK into a Finite State Automata}

The construction of the GCFG with the symbolic system execution showed that the system-calls, the
application logic, and the OSEK semantic can be expressed as a directed graph of all possible
system states. So, the operating system transitions from one state into another one, when an
external event, like a system-call or an external event occurs. Upon the result state an action, in
our case a scheduling decision and a dispatch operation, is done. Therefore, it should be possible
to express the OSEK semantic for exactly this application as a finite state automata.

State automata based approaches to design applications are already widely used in the real-time
systems community. The state-machine compiler~\cite{SMCSite} provides an domain specific language to
express application logic in terms of states and transitions and is able to produce code in various
languages. State machines have one main benefit for verification of systems: they are known to be
turing \emph{in}complete. Therefore, some work was already done to design an operating system as a
state machine~\cite{kim:05:pdcat,NesosSite}.

With the global control-flow graph at hand, a state machine for exactly one application could
be generated that shows exactly the scheduling decisions the OSEK specification demands. By this the
system state could be reduced to architecture specific memory variables and a machine word that
indicates the current system state. For this venture, some tasks have to be tackled: Do all states
of the symbolic system execution need a representation in the operating system state machine? How
can we keep the code size of the state transition table low? What do we have to do in order to cope
with the indeterminism introduced by alarms and events?

\section{Limitations of the Approach}

The presented analysis approach is not only to be extended in the already directions, but it is also
a topic of further research to ease the limitations the current approach reveals. During
\prettyref{cha:system-analysis}, I enlisted already the limitation I demanded from the application
developer. These limitations touched mainly the usage of system calls in various parts of the
applications and the system-call arguments. Besides these internal limitations, the current approach
has several other limitations that narrow the scope of the presented methods.

First of all, not all concepts that are enlisted in the OSEK standard are covered by the
\function{system\_semantic} transformation. Since I restricted the scope of this thesis to OSEK
BCC1, I could omit event support, multiple activations per subtask, and multiple subtasks per
priority. Especially the missing event support is a severe limitation, since it narrows down the
expressiveness of the application code, while the other missing features limit the coarse-grained
structure.

Another limitation of the approach is the complexity of the analysis. As depicted in
\prettyref{sec:evaluation^run-time}, does the SSE analysis exhibit an exponential analysis run
time. The number of system states that have to be enumerated explodes with large applications. This
explosion can either be limited by the presented no-reactivation annotation or by a certain
application design. The closer a system call is to the root of the call hierarchy of a subtask, the
larger can the ABBs be constructed. Therefore, it is a limitation of the approach that it forces a
certain application design, when a short analysis time is required.

Among the exploitation methods, the system-call specialization has the most limited scope. The
general overhead of the system-call decoupling cannot be compensated by the savings the system-call
specialization carries out. Here an hybrid approach might be applicable: Specialize only those
system-calls with very precise system states and use generic implementations for situations with
fuzzy system states.

% - Mentioned limitations
% - Not everything from OSEK is implemented.
%   - Multiple Activations will never be possible
%   - Event Support fehlt
%   - Mehrere Subtasks pro Prioritaet
% - System Call Specialization ist nur fuer spezielle Systeme nuetzlich
% - Komplexe Applikationen brauchen lange Zeit zum analysiseren
\chapter{Conclusion}\label{cha:conclusion}

In this work, the global control-flow graph is presented as a approach for expressing in-depth
application knowledge about real-time systems. Through the tight coupling of application and the
real-time operating system, much of the interaction between those two components can be statically
deduced from the application logic and the operating-system semantic. The OSEK operating-system
standard provides such a semantic and is widely used in the automotive industry.

The global control-flow graph connects the atomic basic blocks, which are a superstructure of the
application's basic blocks, of different subtasks to get a global view on the possible scheduling
and preemption decisions in the system. I provided two approaches, which both operate on an abstract
system state, to calculate the global control-flow graph. The symbolic system execution enumerates
all possible system states, while the system-state flow analysis propagates an fuzzy system state
through the application logic.

The application knowledge can be used to optimize the non functional properties of the operating
system in several ways: The system-call specialization decouples the system-call sites from each
other and exploits the gathered information to remove unnecessary code paths from the kernel
fragments. As a dependability measure, the system-state--assert method inserts checks to verify that
constant parts of the system state have the correct value at certain points in the application. The
control-flow monitoring uses dominator regions within the global control-flow graph to monitor the
execution flow of operating system and application.

{
  \def\CODE#1{/codesize/#1/mean}
  \def\RUNTIME#1{/runtime/#1/mean}
  \def\SDC#1{\sdcP{all}{#1}}

  In the evaluation, I could show the impact of the presented measures upon the \copter benchmark. I
  could construct a encoded \dosek instance that uses
  \drefrel[overhead,abs,percent,base=\CODE{base+enc}]{\CODE{base+enc+opt+ass+flow}} percent less
  flash memory for code, has a
  \drefrel[overhead,percent,abs,base=\RUNTIME{base+enc}]{\RUNTIME{base+enc+opt+ass+flow}} percent
  lower run time and incorporates both system-state asserts and control-flow monitoring. These
  applied measures reduce the silent data corruption rate by
  \drefrel[overhead,abs,percent,base=\SDC{base+mpu+enc}]{\SDC{base+mpu+enc+opt+ass+flow}} percent
  compared to the best, until now, available \dosek.


}

The usage of in-depth application knowledge is one of the keys to software-based measures against
transient hardware faults. The awareness of application behavior opens up the possibility to very
fine-grained dependability measures. But this application knowledge could also be used in other
areas of analyzing and tailoring real-time systems. For example, could a worst-case execution time
analysis benefit strongly from the knowledge about all scheduling decisions. Also, a whole-system
compiler could do constant propagation across kernel activation borders. And the total knowledge of
all system states could provide the possibility to express the whole operating-system logic as a
finite state automata.



%\begin{itemize}
%\item summarize again what your paper did, but now emphasize more the results, and comparisons
%\item write conclusions that can be drawn from the results found and the discussion presented in the paper
%\item future work (be very brief, explain what, but not much how) 
%\end{itemize}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../thesis"
%%% End: 
